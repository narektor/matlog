Log
=========

Log is a free and open-source material-style log reader for Android.

Log is based on [MatLog](https://github.com/plusCubed/matlog), which itself is based on Nolan Lawson's CatLog: [Google Play][1], [GitHub][2]

It shows a scrolling (tailed) view of the Android "logcat" system log, 
hence the goofy name.  It also allows you to record logs in real time, send logs via email, 
and filter using a variety of criteria.

FAQs
-------------
Taken from CatLog's FAQ:

#### Where are the logs saved?

On the SD card, under ```/sdcard/catlog/saved_logs/```.

#### I can't see any logs!

This problem typically shows up on custom ROMs.  First off, try an alternative logging app, to verify that
the problem is with your ROM and not MatLog.

Next, see if your ROM offers system-wide settings to disable logging.  Be sure to reboot after you change anything.

If that still doesn't work, you can contact the creator of your ROM to file a bug/RFE.

License
---------
```
Copyright (C) 2018  Daniel Ciao
Copyright (C) 2022 Narek

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

```

[1]: https://play.google.com/store/apps/details?id=com.nolanlawson.logcat
[2]: https://github.com/nolanlawson/Catlog
       